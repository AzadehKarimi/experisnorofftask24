﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Message =  DateTime.Now.ToString(); 
            return View("MyFirstView");
        }
        public IActionResult SupervisorInfo()
        {
          // Making supervisor list with 3 person in it.
            
            List<Supervisor> supervisorList = new List<Supervisor>();
            Supervisor supervisor1 = new Supervisor();
            supervisor1.Name = "Tom Max";
            supervisor1.Id = 1;
            supervisor1.Level = "Pro";
            supervisor1.IsAvailable = true;
            supervisorList.Add(supervisor1);

            Supervisor supervisor2 = new Supervisor();
            supervisor2.Name = "Sam Jack";
            supervisor2.Id = 2;
            supervisor2.Level = "Pro";
            supervisor2.IsAvailable = true;
            supervisorList.Add(supervisor2);

            Supervisor supervisor3 = new Supervisor();
            supervisor3.Name = "Jim Hans";
            supervisor3.Id = 3;
            supervisor3.Level = "Pro";
            supervisor3.IsAvailable = false;
            supervisorList.Add(supervisor3);




            return View(supervisorList);

        }

    }
}
